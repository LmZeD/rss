@extends('layouts.master')

@section('title')
    Admin index
@endsection

@section('content')
    <div class="container">
        <h1>Admin index page</h1>
        <h2>You are logged in as <strong>{{Auth::user()->email}}</strong></h2>
        @include('partials.sessionErrorSuccessMessages')
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <a href="{{route('admin.categories.index')}}" class="btn btn-outline-primary"
                       onclick="">Categories</a>
                    <a href="{{route('FeedUrl.index')}}" class="btn btn-outline-success" onclick="">Urls</a>
                </div>
            </div>
        </div>
    </div>
@endsection