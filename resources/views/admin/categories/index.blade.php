@extends('layouts.master')

@section('title')
    Categories index
@endsection

@section('content')
    <div class="container">
        <h1>Categories</h1>
        @include('partials.sessionErrorSuccessMessages')
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default padding-20-bottom padding-20-top">
                    <a href="{{route('admin.categories.create')}}" class="btn btn-outline-success">Create new</a>
                </div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Cat. Id</th>
                        <th scope="col">Title</th>
                    </tr>
                    </thead>
                    @foreach($categories as $category)
                        <tbody>
                        <tr>
                            <th scope="row">{{$loop->iteration}}</th>
                            <td>{{$category->id}}</td>
                            <td>{{$category->category_title}}</td>
                        </tr>
                        </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection