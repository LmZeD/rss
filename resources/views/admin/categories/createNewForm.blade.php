@extends('layouts.master')

@section('title')
    Categories index
@endsection

@section('content')
    <div class="container">
        <h1>Categories</h1>
        @include('partials.sessionErrorSuccessMessages')
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default padding-20-bottom padding-20-top">
                    <a href="#" class="btn btn-outline-success">Create new</a>
                </div>
                <form action="{{route('admin.categories.store')}}" method="post">
                    <label for="category_title">Title</label>
                    <input type="text" name="category_title">
                    {{csrf_field()}}
                    <div class="row padding-20-top">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection