@extends('layouts.master')

@section('title')
    Url
@endsection

@section('content')
    <div class="container">
        <h1>Url</h1>
        <h2>Title: {{$url->title}}</h2>
        <h3>Url id: <strong>{{$url->id}}</strong>, {{$url->url}}</h3>
        <p>Category title (id: <strong>{{$url->category_id}}</strong>): {{$url->category_title}}</p>
        <div class="row">
            <div class="col-md-4 col-md-offset-2 padding-20-top">
                <div class="row">
                    <a href="{{route('FeedUrl.edit',['id' => $url->id])}}" class="btn btn-primary">
                        Edit
                    </a>
                    <form action="{{route('FeedUrl.destroy',['id' => $url->id])}}" method="POST"
                          class="padding-20-left">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        {{csrf_field()}}
                        <button class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection