@extends('layouts.master')

@section('title')
    Create category
@endsection

@section('content')
    <div class="container">
        <h1>Create new url</h1>
        <div class="row">
            <div class="col-lg-12">
                <form action="{{route('FeedUrl.store')}}" method="post">
                    <div class="form-control">
                        <label for="title"><strong>Url title</strong></label>
                        <br>
                        <input type="text" placeholder="Title" name="title">
                        <br>

                        <label for="url"><strong>Url</strong></label>
                        <br>
                        <input type="text" placeholder="http://www.example.com/ex" name="url">

                        <label for="description"><strong>Description</strong></label>
                        <br>
                        <input type="text" placeholder="Description" name="description">

                        <label for="category_id"><strong>Description</strong></label>
                        <br>

                        <select name="category_id">
                            <option value="-1">Choose</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->category_title}}</option>
                            @endforeach

                            {{csrf_field()}}
                            <div class="row padding-20-top">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-success ">Submit</button>
                                    <a href="{{route('FeedUrl.index')}}" class="btn btn-primary">Go
                                        back</a>
                                </div>
                            </div>
                        </select>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection