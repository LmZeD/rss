@extends('layouts.master')

@section('title')
    Edit url
@endsection

@section('content')
    <div class="container">
        <h1>Update existing url</h1>
        <div class="row">
            <div class="col-lg-12">
                <form action="{{route('FeedUrl.update',['id' => $url->id])}}" method="post">
                    <div class="form-control">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <label for="title"><strong>Url title</strong></label>
                        <br>
                        <input type="text" value="{{$url->title}}" placeholder="{{$url->title}}" name="title">
                        <br>

                        <label for="url"><strong>Url</strong></label>
                        <br>
                        <input type="text" value="{{$url->url}}" placeholder="{{$url->url}}" name="url">

                        <label for="description"><strong>Description</strong></label>
                        <br>
                        <input type="text" value="{{$url->description}}" placeholder="{{$url->description}}"
                               name="description">

                        <label for="category_id"><strong>Description</strong></label>
                        <br>

                        <select name="category_id">
                            <option value="-1">Choose</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->category_title}}</option>
                            @endforeach

                            {{csrf_field()}}
                            <div class="row padding-20-top">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-success ">Submit</button>
                                    <a href="{{route('FeedUrl.show',['id' => $url->id])}}" class="btn btn-primary">Go
                                        back</a>
                                </div>
                            </div>
                        </select>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection