@extends('layouts.master')

@section('title')
    Url feed CRUD index
@endsection

@section('content')
    <div class="container">
        <h1>Url CRUD page</h1>
        @include('partials.sessionErrorSuccessMessages')
        <div class="row">
            <div class="col-md-12 col-md-offset-2 padding-20-top">
                <a href="{{route('FeedUrl.create')}}" class="btn btn-outline-success">Create new</a>
                <div class="panel panel-default padding-20-top">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Id</th>
                            <th scope="col">Url</th>
                            <th scope="col">Title</th>
                            <th scope="col">Category</th>
                            <th scope="col">Show</th>
                        </tr>
                        </thead>
                        @foreach($urls as $url)
                            <tbody>
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$url->id}}</td>
                                <td>{{$url->url}}</td>
                                <td>{{$url->title}}</td>
                                <td>{{$url->getCategory['category_title']}}</td>
                                <td>
                                    <a href="{{route('FeedUrl.show',['id' => $url->id])}}"
                                       class="btn btn-primary">
                                        Show
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection