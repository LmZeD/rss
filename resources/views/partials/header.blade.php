<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="{{route('getFeed','')}}">RSS</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03"
            aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarHeaderShop">
        <ul class="navbar-nav">
            @auth()
                <a class="nav-link" href="{{route('logout')}}"><i class="fa fa-user" aria-hidden="true"></i> Logout</a>
                <a class="nav-link" href="{{route('admin.index')}}"><i class="fa fa-user"
                                                                       aria-hidden="true"></i>Admin Index</a>
            @else
                <li class="nav-item">
                    <a class="nav-link" href="{{route('login')}}"><i class="fa fa-user" aria-hidden="true"></i>
                        Login</a>
                </li>
            @endauth
        </ul>
    </div>
</nav>
