<div class="modal fade product_view" id="product_view{{$f['id']}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <h3 class="modal-title">{{$f['title']}}</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <p>{!! $f['description'] !!}</p>
                    </div>
                    <div class="col-md-6 col-md-offset-2">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <a href="{{$f['link']}}" class="btn btn-outline-primary" target="_blank">See more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
