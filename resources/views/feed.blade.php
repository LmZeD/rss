@extends('layouts.master')

@section('content')
    <div class="container">
        <h1>Welcome to RSS feed!</h1>
        <div class="row">
            <div class="col-lg-3">
                <h5 class="padding-20-top">Providers list</h5>
                @foreach($feedProviders as $provider)
                    <div class="row">
                        <a href="{{$provider->url}}" target="_blank">&#8226 {{$provider->title}}</a>
                    </div>
                @endforeach
            </div>
            <div class="col-lg-8 col-lg-offset-3 padding-20-top">
                <form id="dropdown" onchange="handleSelect()">
                    <label for="filter">Filter</label>
                    <select name="filter">
                        <option value="Choose">Choose</option>
                        <option value="All">All</option>
                        @foreach($filters as $filter)
                            <option value="{{$filter->category_title}}">{{$filter->category_title}}</option>
                        @endforeach
                    </select>
                </form>
                <p class="padding-20-left">Current filter: <strong>{{$currentFilter}}</strong></p>
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <hr class="max-length">
                        @foreach($feed as $f)
                            @include('partials.modal')
                            <a href="#" data-toggle="modal" data-target="#product_view{{$f['id']}}"><h5
                                        class="text-center">{{$f['title']}}</h5></a>
                            <hr class="max-length">
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script type="text/javascript">
        function handleSelect() {
            var option = $('#dropdown').find(":selected").text();
            window.location.href = '{{URL::to('/feed')}}/' + option;
        }
    </script>
@endsection
