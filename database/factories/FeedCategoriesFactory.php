<?php

use Faker\Generator as Faker;

$factory->define(App\FeedCategory::class, function (Faker $faker) {
    return [
        'category_title' => $faker->sentence()
    ];
});
