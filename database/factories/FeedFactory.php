<?php

use Faker\Generator as Faker;

$factory->define(App\Feed::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(),
        'link' => $faker->url(),
        'description' => $faker->sentence(),
        'publish_date' => $faker->dateTime(),
        'url_id' => $faker->numberBetween(1,5)
    ];
});

