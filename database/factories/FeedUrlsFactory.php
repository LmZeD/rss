<?php

use Faker\Generator as Faker;

$factory->define(App\FeedUrl::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(),
        'url' => $faker->url(),
        'description' => $faker->sentence(),
        'category_id' => $faker->numberBetween(1, 5)
    ];
});
