<?php

use App\FeedUrl;
use Illuminate\Database\Seeder;

class UrlsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FeedUrl::create([
            'title' => 'First one',
            'url' => 'http://feeds.feedburner.com/technologijos-be-spaudos-pranesimu?format=xml',
            'description' => 'RSS feed',
            'category_id' => 1
        ]);

        FeedUrl::create([
            'title' => 'Fake one',
            'url' => 'http://none.com/fake/page',
            'description' => 'RSS feed',
            'category_id' => 2
        ]);

        FeedUrl::create([
            'title' => 'BBC feed',
            'url' => 'http://feeds.bbci.co.uk/news/uk/rss.xml?edition=uk',
            'description' => 'BBC RSS feed',
            'category_id' => 1
        ]);

    }
}
