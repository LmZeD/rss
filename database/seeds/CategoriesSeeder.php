<?php

use App\FeedCategory;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FeedCategory::create([
            'category_title' => 'basic RSS'
        ]);

        FeedCategory::create([
            'category_title' => 'fake RSS'
        ]);
    }
}
