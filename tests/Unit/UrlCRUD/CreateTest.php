<?php

namespace Tests\Unit\UrlCrud;

use App\FeedCategory;
use App\FeedUrl;
use App\Repositories\UrlCRUD\EloquentUrlCRUD;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use PDOException;
use Tests\TestCase;

class CreateTest extends TestCase
{
    use DatabaseTransactions;

    private $urlCRUDRepository;
    private $urlModel;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->urlModel = new FeedUrl();
        $this->urlCRUDRepository = new EloquentUrlCRUD($this->urlModel);
    }

    /**
     * Tests store with valid data provided
     *
     * @return void
     */
    public function testStoreValidDataProvided()
    {
        factory(FeedUrl::class, 5)->make();
        factory(FeedCategory::class, 5)->make();

        $countOfUrls = $this->urlModel->count();

        $this->urlCRUDRepository->store([
            'title' => 'Test',
            'url' => 'http://example.test',
            'description' => 'Lorem ipsum',
            'category_id' => 1
        ]);

        $this->assertEquals($countOfUrls + 1, $this->urlModel->count());
    }

    /**
     * Tests store with invalid data provided
     *
     * @return void
     */
    public function testStoreInvalidDataProvided()
    {
        factory(FeedUrl::class, 5)->create();
        factory(FeedCategory::class, 5)->create();
        $this->expectException(PDOException::class);

        $this->assertInstanceOf(PDOException::class,
            $this->urlCRUDRepository->store([
                'notTitle' => 'Test',
                'notUrl' => 'http://example.test',
                'notDescription' => 'Lorem ipsum',
                'notCategory_id' => 1
            ])
        );
    }
}
