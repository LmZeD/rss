<?php

namespace Tests\Unit\UrlCrud;

use App\FeedUrl;
use App\Repositories\UrlCRUD\EloquentUrlCRUD;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ReadTest extends TestCase
{
    use DatabaseTransactions;

    private $urlCRUDRepository;
    private $urlModel;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->urlModel = new FeedUrl();
        $this->urlCRUDRepository = new EloquentUrlCRUD($this->urlModel);
    }

    /**
     * Tests get all
     *
     * @return void
     */
    public function testGetAll()
    {
        $this->urlCRUDRepository->getAll();

        $this->assertEquals($this->urlModel->count(), count($this->urlCRUDRepository->getAll()));
    }

    /**
     * Tests get by id with non existing id provided
     *
     * @return void
     */
    public function testGetByIdNonExistingIdProvided()
    {
        $this->expectException(ModelNotFoundException::class);
        $this->assertInstanceOf(ModelNotFoundException::class, $this->urlCRUDRepository->getById(2147000000));
    }

    /**
     * Tests get by id with existing id provided
     *
     * @return void
     */
    public function testGetByIdExistingIdProvided()
    {
        factory(FeedUrl::class, 5)->make();

        $this->assertInstanceOf(FeedUrl::class, $this->urlCRUDRepository->getById($this->urlModel->first()->id));
    }
}
