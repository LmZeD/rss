<?php

namespace Tests\Unit\UrlCrud;

use App\FeedUrl;
use App\Repositories\UrlCRUD\EloquentUrlCRUD;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class DeleteTest extends TestCase
{
    use DatabaseTransactions;

    private $urlCRUDRepository;
    private $urlModel;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->urlModel = new FeedUrl();
        $this->urlCRUDRepository = new EloquentUrlCRUD($this->urlModel);
    }

    /**
     * Tests destroy with existing id provided
     *
     * @return void
     */
    public function testDestroyValidIdProvided()
    {
        factory(FeedUrl::class, 5)->make();

        $validId = $this->urlModel->first()->id;
        $countOfUrls = $this->urlModel->count();

        $this->urlCRUDRepository->destroy($validId);

        $this->assertEquals($countOfUrls - 1, $this->urlModel->count());
    }

    /**
     * Tests destroy with non existing id provided
     *
     * @return void
     */
    public function testDestroyNonExistingIdProvided()
    {
        $this->expectException(ModelNotFoundException::class);
        $this->urlCRUDRepository->destroy(2147000000);

        $this->assertInstanceOf(ModelNotFoundException::class, $this->urlCRUDRepository->getById(2147000000));
    }

}
