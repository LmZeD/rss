<?php

namespace Tests\Unit\UrlCrud;

use App\FeedUrl;
use App\Repositories\UrlCRUD\EloquentUrlCRUD;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    use DatabaseTransactions;

    private $urlCRUDRepository;
    private $urlModel;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->urlModel = new FeedUrl();
        $this->urlCRUDRepository = new EloquentUrlCRUD($this->urlModel);
    }

    /**
     * Tests destroy with existing id provided
     *
     * @return void
     */
    public function testUpdateValidIdProvided()
    {
        factory(FeedUrl::class, 5)->make();

        $validId = $this->urlModel->first()->id;

        $this->urlCRUDRepository->update($validId, [
            'title' => 'Test',
            'url' => 'http://example.test',
            'description' => 'Lorem ipsum',
            'category_id' => 1
        ]);

        $this->assertEquals('Test', $this->urlModel->find($validId)->title);
    }

    /**
     * Tests destroy with non existing id provided
     *
     * @return void
     */
    public function testUpdateInvalidIdProvided()
    {
        factory(FeedUrl::class, 5)->make();

        $this->expectException(ModelNotFoundException::class);

        $this->assertInstanceOf(ModelNotFoundException::class, $this->urlCRUDRepository->update(2147000000, [
            'title' => 'Test',
            'url' => 'http://example.test',
            'description' => 'Lorem ipsum',
            'category_id' => 1
        ]));
    }
}
