<?php

namespace Tests\Unit;

use App\FeedCategory;
use App\Services\CreateCategoryService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CreateCategoryServiceTest extends TestCase
{
    use DatabaseTransactions;

    private $createCategoryService;
    private $feedCategoryObject;

    public function __construct(
        ?string $name = null,
        array $data = [],
        string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->feedCategoryObject = new FeedCategory();
        $this->createCategoryService = new CreateCategoryService($this->feedCategoryObject);
    }

    /**
     * Test with valid data provided
     *
     * Size must increase by one and titles must match
     *
     * @return void
     */
    public function testValidData()
    {
        $validDataArray = [
            'category_title' => 'Test'
        ];
        $countBeforeInserting = $this->feedCategoryObject->count();

        $this->createCategoryService->createCategory($validDataArray);

        $countAfterInserting = $this->feedCategoryObject->count();

        $this->assertEquals(
            [$countBeforeInserting + 1, $validDataArray['category_title']],
            [$countAfterInserting, $this->feedCategoryObject->latest()->first()->category_title]
        );
    }

    /**
     * Test with invalid data provided
     *
     * Size must not increase and titles should not match
     *
     * @return void
     */
    public function testInvalidData()
    {
        $validDataArray = [
            'non_existing_column' => 'Test'
        ];
        $countBeforeInserting = $this->feedCategoryObject->count();

        $this->createCategoryService->createCategory($validDataArray);

        $countAfterInserting = $this->feedCategoryObject->count();

        try {//if database is empty
            $lastDatabaseTitle = $this->feedCategoryObject->latest()->first()->category_title;
        } catch (\Exception $e) {
            $lastDatabaseTitle = null;
        }

        $this->assertNotEquals(
            [$countBeforeInserting + 1, $validDataArray['non_existing_column']],
            [$countAfterInserting, $lastDatabaseTitle]
        );
    }
}
