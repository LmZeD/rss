<?php

namespace Tests\Unit;

use App\Feed;
use App\Services\ReadFeedUrlService;
use App\Services\UpdateFeedService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UpdateFeedServiceTest extends TestCase
{
    use DatabaseTransactions;

    private $updateFeedService;
    private $readFeedUrlServiceMock;
    private $feedObject;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->setUpMocks();
        $this->feedObject = new Feed();
        $this->updateFeedService = new UpdateFeedService($this->readFeedUrlServiceMock);
    }

    /**
     * Sets up mocks
     *
     * @return void
     */
    private function setUpMocks()
    {
        $this->readFeedUrlServiceMock = \Mockery::mock(ReadFeedUrlService::class);
    }

    /**
     * Test where readFeedUrl give array of different valid and invalid elements
     *
     * Feeds table must have number of valid elements
     *
     * @return void
     */
    public function testManyDifferentValidAndNotValidElements()
    {
        $generatedArray = [];
        $validAmount = 5;
        $invalidAmount = 5;

        for ($i = 0; $i < $invalidAmount; $i++) {
            $generatedArray[$i] = [
                'notTitle' => 'Test' . $i,
                'notLink' => 'http://test.ts/test' . $i,
                'notDescription' => 'Description of test object' . $i,
                'not_provider_url_id' => 1
            ];
        }

        for ($i = 0; $i < $validAmount; $i++) {
            $generatedArray[$i + $invalidAmount] = [
                'title' => 'Test' . $i,
                'link' => 'http://test.ts/test' . $i,
                'description' => 'Description of test object' . $i,
                'provider_url_id' => 1
            ];
        }

        $this->feedObject->truncate();
        $this->readFeedUrlServiceMock->shouldReceive('readAllUrls')->once()->andReturn($generatedArray);
        $this->updateFeedService->updateFeed();

        $this->assertEquals($validAmount, $this->feedObject->count());
    }

    /**
     * Test where readFeedUrl give array of different elements
     *
     * Feeds table must have zero elements
     *
     * @return void
     */
    public function testManyDifferentNotValidElements()
    {
        $generatedArray = [];
        $amount = 5;

        for ($i = 0; $i < $amount; $i++) {
            $generatedArray[$i] = [
                'notTitle' => 'Test' . $i,
                'notLink' => 'http://test.ts/test' . $i,
                'notDescription' => 'Description of test object' . $i,
                'not_provider_url_id' => 1
            ];
        }

        $this->feedObject->truncate();
        $this->readFeedUrlServiceMock->shouldReceive('readAllUrls')->once()->andReturn($generatedArray);
        $this->updateFeedService->updateFeed();

        $this->assertEquals(0, $this->feedObject->count());
    }

    /**
     * Test where readFeedUrl give array of different elements
     *
     * Feeds table must have many elements
     *
     * @return void
     */
    public function testManyDifferentElements()
    {
        $generatedArray = [];
        $amount = 50;

        for ($i = 0; $i < $amount; $i++) {
            $generatedArray[$i] = [
                'title' => 'Test' . $i,
                'link' => 'http://test.ts/test' . $i,
                'description' => 'Description of test object' . $i,
                'provider_url_id' => 1
            ];
        }

        $this->feedObject->truncate();
        $this->readFeedUrlServiceMock->shouldReceive('readAllUrls')->once()->andReturn($generatedArray);
        $this->updateFeedService->updateFeed();

        $this->assertEquals($amount, $this->feedObject->count());
    }

    /**
     * Test where readFeedUrl give array of same elements
     *
     * Feeds table must have one element
     *
     * @return void
     */
    public function testManySameElements()
    {
        $generatedArray = [];
        $amount = 5;

        for ($i = 0; $i < $amount; $i++) {
            $generatedArray[$i] = [
                'title' => 'Test',
                'link' => 'http://test.ts/test',
                'description' => 'Description of test object',
                'provider_url_id' => 1
            ];
        }

        $this->feedObject->truncate();
        $this->readFeedUrlServiceMock->shouldReceive('readAllUrls')->once()->andReturn($generatedArray);
        $this->updateFeedService->updateFeed();

        $this->assertEquals(1, $this->feedObject->count());
    }

    /**
     * Test where readFeedUrl doesn't give anything
     *
     * Feeds table must be empty
     *
     * @return void
     */
    public function testNothingToUpdate()
    {
        $this->feedObject->truncate();
        $this->readFeedUrlServiceMock->shouldReceive('readAllUrls')->once()->andReturn([]);
        $this->updateFeedService->updateFeed();

        $this->assertEmpty($this->feedObject->all());
    }
}
