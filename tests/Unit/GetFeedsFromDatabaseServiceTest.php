<?php

namespace Tests\Unit;

use App\Feed;
use App\FeedCategory;
use App\Services\GetFeedsFromDatabaseService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class GetFeedsFromDatabaseServiceTest extends TestCase
{
    use DatabaseTransactions;

    private $getFeedsFromDatabaseService;
    private $feedObject;
    private $feedCategoryObject;

    public function __construct(
        string $name = null,
        array $data = [],
        string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->feedObject = new Feed();
        $this->feedCategoryObject = new FeedCategory();
        $this->getFeedsFromDatabaseService = new GetFeedsFromDatabaseService($this->feedObject, $this->feedCategoryObject);
    }

    /**
     * Test with non existing string as filter
     *
     * Must get all feeds
     *
     * @return void
     */
    public function testInvalidFilter()
    {
        $filter = '_!Non__Existing__Title!_';// 1/10000000 that someone will name it that way

        $collectedFeeds = $this->getFeedsFromDatabaseService->getFeeds($filter);

        $this->assertEquals(0, count($collectedFeeds));
    }

    /**
     * Test with random existing filter
     *
     * Must get all feeds of given filter
     *
     * @return void
     */
    public function testValidDataRandomExistingFiler()
    {
        $categories = $this->feedCategoryObject->all();

        $totalFeedCount = 0;
        $filter = '_!Non__Existing__Title!_';// 1/10000000 that someone will name it that way
        // (it's placeholder in case database is empty)

        if (count($categories) > 0) { //if database is not empty
            $randomId = rand(0, count($categories) - 1);
            $filter = $categories[$randomId]->category_title;
            foreach ($categories[$randomId]->getFeedUrls as $url) {
                $totalFeedCount = $totalFeedCount + $this->feedObject->where('url_id', $url->id)->get()->count();
            }
        }

        $collectedFeeds = $this->getFeedsFromDatabaseService->getFeeds($filter);

        $this->assertEquals($totalFeedCount, count($collectedFeeds));
    }

    /**
     * Test with null as filter
     *
     * Must get all feeds
     *
     * @return void
     */
    public function testValidDataNullFilter()
    {
        $totalFeedCount = $this->feedObject->count();

        $collectedFeeds = $this->getFeedsFromDatabaseService->getFeeds(null);

        $this->assertEquals($totalFeedCount, $collectedFeeds->count());
    }

    /**
     * Test with empty string as filter
     *
     * Must get all feeds
     *
     * @return void
     */
    public function testValidDataEmptyStringFilter()
    {
        $this->app->make('db')->beginTransaction();

        $totalFeedCount = $this->feedObject->count();

        $collectedFeeds = $this->getFeedsFromDatabaseService->getFeeds('');

        $this->assertEquals($totalFeedCount, $collectedFeeds->count());

        $this->beforeApplicationDestroyed(function () {
            $this->app->make('db')->rollBack();
        });

    }
}
