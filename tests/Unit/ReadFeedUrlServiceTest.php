<?php

namespace Tests\Unit;

use App\Feed;
use App\FeedUrl;
use App\Services\ReadFeedUrlService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;


class ReadFeedUrlServiceTest extends TestCase
{
    use DatabaseTransactions;

    private $readFeedUrlService;
    private $urlObject;
    private $feedObject;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->urlObject = new FeedUrl();
        $this->feedObject = new Feed();
        $this->readFeedUrlService = new ReadFeedUrlService($this->urlObject);
    }

    /**
     * Tests if readAllUrls gets all urls
     *
     * Result must be equal size as all database elements
     *
     * If it's not it means urls have been updated and our database is outdated
     * Remove comments to re-load database just for this test
     * Commented code because otherwise this test would depend on other service
     * Now, it requires up to date database
     *
     * @return void
     */
    public function testReadAllUrls()
    {
        $feedsCount = $this->feedObject->count();
        $readUrlsResult = $this->readFeedUrlService->readAllUrls();

        //READ COMMENTS!!!
        $this->assertEquals($feedsCount, count($readUrlsResult));
    }
}
