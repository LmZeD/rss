<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/feed', 302);
Route::redirect('/home', '/feed', 302);

Auth::routes();

Route::get('/logout', [
    'uses' => 'Auth\LoginController@logout',
    'as' => 'logout'
]);

Route::get('/feed', [
    'uses' => 'FeedController@index',
    'as' => 'getFeed'
]);

Route::get('/feed/{filter}', [
    'uses' => 'FeedController@index',
    'as' => 'getFeed'
]);

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::view('/', 'admin.index')->name('admin.index');
    Route::get('/categories', 'FeedCategoryController@index')->name('admin.categories.index');
    Route::post('/categories/store', 'FeedCategoryController@store')->name('admin.categories.store');
    Route::view('/categories/create', 'admin.categories.createNewForm')->name('admin.categories.create');
    Route::view('/urls', 'admin.urls.index')->name('admin.urls.index');
    Route::resource('FeedUrl', 'FeedUrlController');
});