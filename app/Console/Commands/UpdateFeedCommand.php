<?php

namespace App\Console\Commands;

use App\Feed;
use App\Services\UpdateFeedService;
use Illuminate\Console\Command;

class UpdateFeedCommand extends Command
{
    private $updateFeedService;
    private $feedObj;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates feeds from provider urls';

    /**
     * Create a new command instance.
     *
     * @param UpdateFeedService $updateFeedService
     * @param Feed $feedObj
     *
     * @return void
     */
    public function __construct(UpdateFeedService $updateFeedService, Feed $feedObj)
    {
        parent::__construct();
        $this->updateFeedService = $updateFeedService;
        $this->feedObj = $feedObj;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->output->writeln('Starting...');

        if ($this->getFeedsCount() > 0) {
            $prompt = $this->ask('Clear stored feeds? (y/n)');
            if (strtolower($prompt) === 'yes' || strtolower($prompt) === 'y') {
                $this->clearFeeds();
            }
        }
        $this->updateFeedService->updateFeed();

        $this->output->writeln('Great success!');
    }

    private function getFeedsCount()
    {
        return $this->feedObj->count();
    }

    private function clearFeeds()
    {
        $this->feedObj->truncate();
    }
}
