<?php

namespace App\Console\Commands;

use Validator;
use App\Admin;
use Illuminate\Console\Command;

class CreateAdminUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {name?} {email?} {password?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates admin user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        [$name, $email, $password] = $this->setData();

        $validation = $this->validateEmail($email);
        if ($validation->fails()) {
            $this->output->writeln('Provided email is invalid or is already registered(' . $email . ')');
            exit(0);
        }
        $this->output->writeln('Creating admin user (' . $name . ') with email: ' . $email);

        try {
            $this->createAdminUser($name, $email, $password);
        } catch (\Exception $e) {
            $this->output->writeln('Fatal error.');
            exit(0);
        }
        $this->output->writeln('Success, user created');
    }

    /**
     * Sets data from fields or asks to fill in values
     *
     *
     * @return array
     */
    private function setData()
    {
        $email = $this->argument('email');
        $password = $this->argument('password');
        $name = $this->argument('name');
        if ($name == null) {
            $name = $this->ask('Please enter your name');
        }
        if ($password === null) {
            $password = $this->ask('Please enter your password');
        }
        if ($email == null) {
            $email = $this->ask('Please enter your email');
        }
        return [$name, $email, $password];
    }

    /**
     * Validate email
     *
     * @param $email
     *
     * @return Validator
     */
    private function validateEmail($email)
    {
        return Validator::make(['email' => $email], [
            'email' => 'unique:admins|email'
        ]);
    }

    /**
     * Create admin user
     *
     * @param $name
     * @param $email
     * @param $password
     *
     * @return Admin
     */
    private function createAdminUser($name, $email, $password)
    {
        return Admin::create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password),
        ]);
    }
}
