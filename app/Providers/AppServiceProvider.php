<?php

namespace App\Providers;

use App\Repositories\DatabaseFiller\FactoryFillDatabase;
use App\Repositories\DatabaseFiller\FillDatabase;
use App\Repositories\UrlCRUD\EloquentUrlCRUD;
use App\Repositories\UrlCRUD\UrlCRUD;
use App\Services\CreateCategoryService;
use App\Services\GetFeedsFromDatabaseService;
use App\Services\ReadFeedUrlService;
use App\Services\UpdateFeedService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FillDatabase::class, FactoryFillDatabase::class);
        $this->app->bind(UrlCRUD::class, EloquentUrlCRUD::class);
        $this->app->bind('CreateCategoryService', CreateCategoryService::class);
        $this->app->bind('UpdateFeedService', UpdateFeedService::class);
        $this->app->bind('ReadFeedUrlService', ReadFeedUrlService::class);
        $this->app->bind('GetFeedsFromDatabaseService', GetFeedsFromDatabaseService::class);
    }
}
