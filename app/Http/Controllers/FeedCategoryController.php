<?php

namespace App\Http\Controllers;

use App\FeedCategory;
use App\Http\Requests\FeedCategoryRequest;
use App\Services\CreateCategoryService;
use Illuminate\Http\Response;
use Illuminate\View\View;

class FeedCategoryController extends Controller
{
    private $createCategoryService;
    private $categoriesObj;

    public function __construct(CreateCategoryService $createCategoryService, FeedCategory $feedCategory)
    {
        $this->createCategoryService = $createCategoryService;
        $this->categoriesObj = $feedCategory;
    }

    /**
     * Display index
     *
     * @return View
     */
    public function index(): View
    {
        $categories = $this->categoriesObj->all();
        return view('admin.categories.index', ['categories' => $categories]);
    }

    /**
     * Create new category
     *
     * @param FeedCategoryRequest $request
     *
     * @return Response
     */
    public function store(FeedCategoryRequest $request): Response
    {
        $this->createCategoryService->createCategory($request->all());
        return redirect()->route('admin.categories.index')->with('success', 'Successfully created category!');
    }
}
