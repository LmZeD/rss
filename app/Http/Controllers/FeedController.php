<?php

namespace App\Http\Controllers;

use App\FeedCategory;
use App\FeedUrl;
use App\Services\GetFeedsFromDatabaseService;
use Illuminate\View\View;

class FeedController extends Controller
{
    private $getFeedsFromDatabaseService;
    private $feedCategoryObject;
    private $feedUrlsObject;

    public function __construct(
        GetFeedsFromDatabaseService $getFeedsFromDatabaseService,
        FeedCategory $feedCategoryObject,
        FeedUrl $feedUrlsObject
    )
    {
        $this->feedCategoryObject = $feedCategoryObject;
        $this->getFeedsFromDatabaseService = $getFeedsFromDatabaseService;
        $this->feedUrlsObject = $feedUrlsObject;
    }

    /**
     * Display a listing of the resource.
     *
     * @param string $filter
     *
     * @return View
     */
    public function index(string $filter = null): View
    {
        $filters = $this->feedCategoryObject->all();
        $feed = $this->getFeedsFromDatabaseService->getFeeds($filter);
        $feedProviders = $this->feedUrlsObject->all();

        return view('feed',
            [
                'feed' => $feed,
                'currentFilter' => $filter,
                'filters' => $filters,
                'feedProviders' => $feedProviders
            ]);
    }
}
