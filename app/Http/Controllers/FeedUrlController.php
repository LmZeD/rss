<?php

namespace App\Http\Controllers;

use App\FeedCategory;
use App\Http\Requests\FeedUrlRequest;
use App\Repositories\UrlCRUD\EloquentUrlCRUD;
use Illuminate\Http\Response;
use Illuminate\View\View;

class FeedUrlController extends Controller
{
    private $urlCRUD;
    private $categoryObj;

    public function __construct(EloquentUrlCRUD $urlCRUD, FeedCategory $categoryObj)
    {
        $this->urlCRUD = $urlCRUD;
        $this->categoryObj = $categoryObj;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $urls = $this->urlCRUD->getAll();
        return view('admin.urls.index', ['urls' => $urls]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $categories = $this->categoryObj->all();
        return view('admin.urls.createNewUrlForm', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  FeedUrlRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(FeedUrlRequest $request): Response
    {
        $this->urlCRUD->store($request->all());
        return redirect()->route('FeedUrl.index')->with('success', 'Successfully created url!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return View
     */
    public function show(int $id): View
    {
        $url = $this->urlCRUD->getById($id);
        $category = $this->categoryObj->where('id', $url->category_id)->select('category_title')->first();
        $url->category_title = $category['category_title'];
        return view('admin.urls.showSingleItem', ['url' => $url]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return View
     */
    public function edit(int $id): View
    {
        $url = $this->urlCRUD->getById($id);
        $categories = $this->categoryObj->all();
        return view('admin.urls.editForm', ['url' => $url, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param FeedUrlRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(FeedUrlRequest $request, int $id): Response
    {
        $this->urlCRUD->update($id, $request->all());
        return redirect()->route('FeedUrl.index')->with('success', 'Successfully updated url!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id): Response
    {
        $this->urlCRUD->destroy($id);
        return redirect()->route('FeedUrl.index')->with('success', 'Successfully deleted url!');
    }
}
