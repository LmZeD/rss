<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FeedUrlRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'url' => 'required|url',
            'description' => 'required|string',
            'category_id' => 'required|integer|min:0'
        ];
    }

    public function messages()
    {
        return [
            'title' => 'Title is required and must be string',
            'url' => 'Url is required and must be url',
            'description' => 'Description is required and must be string',
            'category_id' => 'Category id is required and must be int greater or equal to 0'
        ];
    }
}
