<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedUrl extends Model
{
    protected $fillable = [
        'title', 'url', 'description', 'category_id'
    ];

    public function getCategory()
    {
        return $this->belongsTo(FeedCategory::class, 'category_id', 'id');
    }

    public function getFeeds()
    {
        return $this->hasMany(Feed::class, 'url_id', 'id');
    }
}
