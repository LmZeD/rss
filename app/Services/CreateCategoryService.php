<?php

namespace App\Services;

use App\FeedCategory;

class CreateCategoryService
{
    private $categoryModel;

    public function __construct(FeedCategory $categoryModel)
    {
        $this->categoryModel = $categoryModel;
    }

    /**
     * Try to create Category object by given values
     *
     * @param $values
     *
     * @return FeedCategory
     */
    public function createCategory(array $values): ? FeedCategory
    {
        try {
            return $this->categoryModel->create($values);
        } catch (\Exception $ex) {
            return null;
        }
    }
}