<?php

namespace App\Services;

use App\FeedUrl;

class ReadFeedUrlService
{
    private $urlObject;

    public function __construct(FeedUrl $urlObject)
    {
        $this->urlObject = $urlObject;
    }

    /**
     * Gets feed from given url
     *
     * @param FeedUrl $url
     *
     * @return array
     */
    private function getContentFromUrl(FeedUrl $url)
    {
        try {
            $html = file_get_contents($url->url);
            $xml = simplexml_load_string($html, null, LIBXML_NOCDATA);
            $rezArr = $xml->children();
            $array = json_decode(json_encode($rezArr), true);
        } catch (\Exception $e) {//bad link or format
            return [];
        }

        $arrayOfItems = $this->giveProviderIdToItems($url, $array['channel']['item']);

        return $arrayOfItems;
    }


    /**
     * Gets feed from given url
     *
     * @param FeedUrl $url
     * @param array $arrayOfItems
     *
     * @return array
     */
    private function giveProviderIdToItems(FeedUrl $url, $arrayOfItems)
    {
        $i=0;
        $arrayWithProviderId = [];
        foreach ($arrayOfItems as $item) {
            $item['provider_url_id'] = $url->id;
            $arrayWithProviderId[$i++] = $item;
        }

        return $arrayWithProviderId;
    }

    /**
     * Reads all feed from given urls
     *
     * @return array
     */
    public function readAllUrls()
    {
        $mergedArrays = [];
        $urls = $this->urlObject->all();

        foreach ($urls as $url) {
            $array = $this->getContentFromUrl($url);
            $mergedArrays = array_merge($mergedArrays, $array);
        }
        return $mergedArrays;
    }
}