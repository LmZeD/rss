<?php

namespace App\Services;

use App\Feed;
use App\FeedCategory;

class GetFeedsFromDatabaseService
{
    private $feedObject;
    private $feedCategoryObject;

    public function __construct(Feed $feedObject, FeedCategory $feedCategoryObject)
    {
        $this->feedObject = $feedObject;
        $this->feedCategoryObject = $feedCategoryObject;
    }

    /**
     * Gets feed from database by given filter
     *
     * @param string $filter
     *
     * @return array
     */
    public function getFeeds($filter)
    {
        switch ($filter) {
            case 'All':
                $feeds = $this->getAll();
                break;
            case '':
                $feeds = $this->getAll();
                break;
            default:
                $feeds = $this->getAllOfType($filter);
                break;
        }

        return $feeds;
    }

    /**
     * Gets all feed from database by given filter
     *
     * @param string $filter
     *
     * @return array
     */
    private function getAllOfType(string $filter)
    {
        $mergedFeeds = [];
        $feeds = $this->feedCategoryObject->with('getFeedUrls')->where('category_title', $filter)
            ->get();
        if ($feeds->isEmpty() == false) {

            foreach ($feeds[0]->getFeedUrls as $feedUrl) {
                $collection = $feedUrl->getFeeds()->get();
                $array = $collection->toArray();
                $mergedFeeds = array_merge($mergedFeeds, $array);
            }
        }
        return $mergedFeeds;
    }

    /**
     * Gets all feed from database
     *
     * @return array
     */
    private function getAll()
    {
        $feeds = $this->feedObject->all();

        return $feeds;
    }
}