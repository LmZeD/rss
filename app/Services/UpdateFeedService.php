<?php

namespace App\Services;

use App\Feed;
use Carbon\Carbon;

class UpdateFeedService
{
    private $readFeedUrlService;

    public function __construct(ReadFeedUrlService $readFeedUrlService)
    {
        $this->readFeedUrlService = $readFeedUrlService;
    }

    /**
     * Updates feed
     *
     * @return boolean
     */
    public function updateFeed()
    {
        $mergedArrays = $this->readFeedUrlService->readAllUrls();
        foreach ($mergedArrays as $element) {
            $element = $this->fixFieldsIfNeeded($element);
            try {
                Feed::create([
                    'title' => $element['title'],
                    'link' => $element['link'],
                    'description' => $element['description'],
                    'url_id' => $element['provider_url_id'],
                    'publish_date' => $element['pubDate'],
                ]);
            } catch (\Exception $ex) { //not unique or invalid data, skip
                continue;
            }
        }

        return true;
    }

    /**
     * Fixes possible input failures
     *
     * @param array $element
     *
     * @return array
     */
    private function fixFieldsIfNeeded($element)
    {
        if (array_key_exists('description', $element) && is_array($element['description'])) {
            $element['description'] = implode(' ', $element['description']);
        }
        if (array_key_exists('pubDate', $element) != true || $element['pubDate'] == null) {
            $element['pubDate'] = Carbon::now()->toDateTimeString();
        }
        return $element;
    }
}