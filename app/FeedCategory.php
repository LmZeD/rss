<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedCategory extends Model
{
    protected $fillable = [
        'category_title'
    ];

    public function getFeedUrls()
    {
        return $this->hasMany(FeedUrl::class, 'category_id', 'id');
    }
}
