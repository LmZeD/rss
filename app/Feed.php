<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    protected $fillable = ['title', 'link', 'description', 'publish_date', 'url_id'];

    public function getFeedUrl()
    {
        return $this->belongsTo(FeedUrl::class, 'url_id', 'id');
    }
}
