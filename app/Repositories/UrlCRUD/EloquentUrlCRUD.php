<?php

namespace App\Repositories\UrlCRUD;

use App\FeedUrl;
use Illuminate\Support\Collection;

class EloquentUrlCRUD implements UrlCRUD
{
    private $model;

    public function __construct(FeedUrl $model)
    {
        return $this->model = $model;
    }

    /**
     * Store FeedUrl to database
     *
     * @param array $values
     *
     * @return void
     */
    public function store(array $values)
    {
        $this->model->create($values);
    }

    /**
     * Gets FeedUrl object or displays error
     *
     * @param int $id
     *
     * @return FeedUrl
     */
    public function getById(int $id): FeedUrl
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Deletes object from database or displays error
     *
     * @param int $id
     *
     * @return boolean
     */
    public function destroy(int $id): bool
    {
        $this->model->findOrFail($id)->delete();
        return true;
    }

    /**
     * Updates object or displays error
     *
     * @param int $id
     * @param array $values
     *
     * @return FeedUrl
     */
    public function update(int $id, array $values): FeedUrl
    {
        $obj = $this->model->findOrFail($id);
        $obj->update($values);
        return $obj;
    }

    /**
     * Returns collection of FeedUrls and their categories
     *
     * @return Collection
     */
    public function getAll(): Collection
    {
        $obj = $this->model->with('getCategory')->get();
        return $obj;
    }
}