<?php

namespace App\Repositories\UrlCRUD;

interface UrlCRUD
{
    public function store(array $values);

    public function getById(int $id);

    public function update(int $id, array $values);

    public function destroy(int $id);

    public function getAll();
}