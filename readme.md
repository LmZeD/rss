Setting up pre run:
    -composer install
    -(in RSS directory) touch database/database.sqlite
    -create .env file and fill with your credentials (like in .env.example)
    -php artisan key:generate
    -php artisan migrate
    -php artisan db:seed
    -php artisan feed:update
    
How to run:
    -php artisan serve
        *default port is 8000, to change php artisan serve --port=yourDesiredPort    

Setting up, testing:
    -Some valid data for testing out
        * php artisan db:seed (in case you forgot)
    -Running tests
        * /vendor/bin/phpunit
        
Comments:        
    Creating model object in services:
        -Not quite used to do that, I rather use Model:: approach, but some say it's good practice      
    Dependency injection:
        -This wasn't heavy calculation app, mostly db requests, but I managed to pull out service to use other service
    Tests:
        -Well, I guess unit tests aren't the most efficient, but it gets the job done, all in all, my test 
        writing skills should improve
        -Wasn't sure if I should write Selenium tests, so I didn't (it wouldn't impress, because it's very basic app)
    Front-end:
        -Used bootstrap, only basic styles just to "get data on screen"
    How it works:
        -SimpleXml reads url, converts data to json, objects created with data from json. Works with any website running
        xml rss feed.
    Tools/libraries used:
        -None